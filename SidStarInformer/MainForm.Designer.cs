﻿namespace SidStarInformer
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.GroupBox grpNavDataSource;
			System.Windows.Forms.Label lblNavDataSource;
			System.Windows.Forms.GroupBox grpInterests;
			System.Windows.Forms.Label lblIcaoCodes;
			this.btnBrowseForNavDataPath = new System.Windows.Forms.Button();
			this.txtNavDataPath = new System.Windows.Forms.TextBox();
			this.chckGenerateSidTransitions = new System.Windows.Forms.CheckBox();
			this.chckGenerateSid = new System.Windows.Forms.CheckBox();
			this.chckGenerateStarTransitions = new System.Windows.Forms.CheckBox();
			this.chckGenerateStar = new System.Windows.Forms.CheckBox();
			this.txtIcaoCodes = new System.Windows.Forms.TextBox();
			this.btnGenerate = new System.Windows.Forms.Button();
			grpNavDataSource = new System.Windows.Forms.GroupBox();
			lblNavDataSource = new System.Windows.Forms.Label();
			grpInterests = new System.Windows.Forms.GroupBox();
			lblIcaoCodes = new System.Windows.Forms.Label();
			grpNavDataSource.SuspendLayout();
			grpInterests.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpNavDataSource
			// 
			grpNavDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			grpNavDataSource.Controls.Add(this.btnBrowseForNavDataPath);
			grpNavDataSource.Controls.Add(this.txtNavDataPath);
			grpNavDataSource.Controls.Add(lblNavDataSource);
			grpNavDataSource.Location = new System.Drawing.Point(7, 8);
			grpNavDataSource.Name = "grpNavDataSource";
			grpNavDataSource.Size = new System.Drawing.Size(497, 73);
			grpNavDataSource.TabIndex = 3;
			grpNavDataSource.TabStop = false;
			grpNavDataSource.Text = "Навигационные данные";
			// 
			// btnBrowseForNavDataPath
			// 
			this.btnBrowseForNavDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBrowseForNavDataPath.Location = new System.Drawing.Point(406, 41);
			this.btnBrowseForNavDataPath.Name = "btnBrowseForNavDataPath";
			this.btnBrowseForNavDataPath.Size = new System.Drawing.Size(82, 23);
			this.btnBrowseForNavDataPath.TabIndex = 3;
			this.btnBrowseForNavDataPath.Text = "Указать...";
			this.btnBrowseForNavDataPath.UseVisualStyleBackColor = true;
			this.btnBrowseForNavDataPath.Click += new System.EventHandler(this.btnBrowseForNavDataPath_Click);
			// 
			// txtNavDataPath
			// 
			this.txtNavDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtNavDataPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtNavDataPath.Location = new System.Drawing.Point(7, 42);
			this.txtNavDataPath.Name = "txtNavDataPath";
			this.txtNavDataPath.Size = new System.Drawing.Size(390, 20);
			this.txtNavDataPath.TabIndex = 1;
			// 
			// lblNavDataSource
			// 
			lblNavDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			lblNavDataSource.AutoSize = true;
			lblNavDataSource.Location = new System.Drawing.Point(4, 24);
			lblNavDataSource.Name = "lblNavDataSource";
			lblNavDataSource.Size = new System.Drawing.Size(249, 13);
			lblNavDataSource.TabIndex = 0;
			lblNavDataSource.Text = "Путь к базе KLN90B (папка \"KLN 90B Navdata\")";
			// 
			// grpInterests
			// 
			grpInterests.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			grpInterests.Controls.Add(this.chckGenerateSidTransitions);
			grpInterests.Controls.Add(this.chckGenerateSid);
			grpInterests.Controls.Add(this.chckGenerateStarTransitions);
			grpInterests.Controls.Add(this.chckGenerateStar);
			grpInterests.Controls.Add(lblIcaoCodes);
			grpInterests.Controls.Add(this.txtIcaoCodes);
			grpInterests.Location = new System.Drawing.Point(8, 96);
			grpInterests.Name = "grpInterests";
			grpInterests.Size = new System.Drawing.Size(494, 136);
			grpInterests.TabIndex = 4;
			grpInterests.TabStop = false;
			grpInterests.Text = "Область интереса";
			// 
			// chckGenerateSidTransitions
			// 
			this.chckGenerateSidTransitions.AutoSize = true;
			this.chckGenerateSidTransitions.Location = new System.Drawing.Point(8, 104);
			this.chckGenerateSidTransitions.Name = "chckGenerateSidTransitions";
			this.chckGenerateSidTransitions.Size = new System.Drawing.Size(94, 17);
			this.chckGenerateSidTransitions.TabIndex = 2;
			this.chckGenerateSidTransitions.Text = "SID transitions";
			this.chckGenerateSidTransitions.UseVisualStyleBackColor = true;
			// 
			// chckGenerateSid
			// 
			this.chckGenerateSid.AutoSize = true;
			this.chckGenerateSid.Location = new System.Drawing.Point(8, 80);
			this.chckGenerateSid.Name = "chckGenerateSid";
			this.chckGenerateSid.Size = new System.Drawing.Size(44, 17);
			this.chckGenerateSid.TabIndex = 1;
			this.chckGenerateSid.Text = "SID";
			this.chckGenerateSid.UseVisualStyleBackColor = true;
			// 
			// chckGenerateStarTransitions
			// 
			this.chckGenerateStarTransitions.AutoSize = true;
			this.chckGenerateStarTransitions.Location = new System.Drawing.Point(160, 104);
			this.chckGenerateStarTransitions.Name = "chckGenerateStarTransitions";
			this.chckGenerateStarTransitions.Size = new System.Drawing.Size(105, 17);
			this.chckGenerateStarTransitions.TabIndex = 4;
			this.chckGenerateStarTransitions.Text = "STAR transitions";
			this.chckGenerateStarTransitions.UseVisualStyleBackColor = true;
			// 
			// chckGenerateStar
			// 
			this.chckGenerateStar.AutoSize = true;
			this.chckGenerateStar.Location = new System.Drawing.Point(160, 80);
			this.chckGenerateStar.Name = "chckGenerateStar";
			this.chckGenerateStar.Size = new System.Drawing.Size(55, 17);
			this.chckGenerateStar.TabIndex = 3;
			this.chckGenerateStar.Text = "STAR";
			this.chckGenerateStar.UseVisualStyleBackColor = true;
			// 
			// lblIcaoCodes
			// 
			lblIcaoCodes.AutoSize = true;
			lblIcaoCodes.Location = new System.Drawing.Point(6, 24);
			lblIcaoCodes.Name = "lblIcaoCodes";
			lblIcaoCodes.Size = new System.Drawing.Size(175, 13);
			lblIcaoCodes.TabIndex = 1;
			lblIcaoCodes.Text = "Список аэропортов по коду ICAO";
			// 
			// txtIcaoCodes
			// 
			this.txtIcaoCodes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtIcaoCodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtIcaoCodes.Location = new System.Drawing.Point(8, 43);
			this.txtIcaoCodes.Name = "txtIcaoCodes";
			this.txtIcaoCodes.Size = new System.Drawing.Size(387, 20);
			this.txtIcaoCodes.TabIndex = 0;
			// 
			// btnGenerate
			// 
			this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGenerate.Location = new System.Drawing.Point(8, 240);
			this.btnGenerate.Name = "btnGenerate";
			this.btnGenerate.Size = new System.Drawing.Size(496, 32);
			this.btnGenerate.TabIndex = 6;
			this.btnGenerate.Text = "Сгенерировать...";
			this.btnGenerate.UseVisualStyleBackColor = true;
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
			this.ClientSize = new System.Drawing.Size(511, 287);
			this.Controls.Add(this.btnGenerate);
			this.Controls.Add(grpInterests);
			this.Controls.Add(grpNavDataSource);
			this.DoubleBuffered = true;
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(527, 326);
			this.Name = "MainForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SID / STAR Informer";
			this.Load += new System.EventHandler(this.MainForm_Load);
			grpNavDataSource.ResumeLayout(false);
			grpNavDataSource.PerformLayout();
			grpInterests.ResumeLayout(false);
			grpInterests.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button btnBrowseForNavDataPath;
		private System.Windows.Forms.TextBox txtNavDataPath;
		private System.Windows.Forms.TextBox txtIcaoCodes;
		private System.Windows.Forms.CheckBox chckGenerateStarTransitions;
		private System.Windows.Forms.CheckBox chckGenerateStar;
		private System.Windows.Forms.CheckBox chckGenerateSidTransitions;
		private System.Windows.Forms.CheckBox chckGenerateSid;
		private System.Windows.Forms.Button btnGenerate;
	}
}

