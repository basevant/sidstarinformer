﻿using System;
using System.Configuration;
using System.IO;

namespace SidStarInformer.Configuration
{
	internal static class Configurator
	{
		internal static string IcaoCodesList
			=> ReadStringValue(nameof(IcaoCodesList));

		internal static bool GenerateSid
			=> ReadBoolValue(nameof(GenerateSid));

		internal static bool GenerateSidTransitions
			=> ReadBoolValue(nameof(GenerateSidTransitions));

		internal static bool GenerateStar
			=> ReadBoolValue(nameof(GenerateStar));

		internal static bool GenerateStarTransitions
			=> ReadBoolValue(nameof(GenerateStarTransitions));

		internal static string GetKln90BNavDataPath()
		{
			var configuredPath = ReadStringValue("Kln90BNavDataPath");

			if (Directory.Exists(configuredPath))
			{
				return configuredPath;
			}

			//	we have no configured nav data path value,
			//	try to enumerate X-Plane instances to find the KLN90B nav data
			//

			//	check the installed '< X-Plane 10' instances
			//

			var retVal = FindFirstNavDataFolderInstance(
				BuildXPlaneInstancesFilePath(true)
				);

			if (Directory.Exists(retVal))
			{
				return retVal;
			}

			//	check the installed '>= X-Plane 10' instances
			//

			retVal = FindFirstNavDataFolderInstance(
				BuildXPlaneInstancesFilePath(false)
				);

			return (
				Directory.Exists(retVal)
				? retVal
				: string.Empty
				);
		}

		internal static bool SaveUiSettings(
			string navDataPath,
			string icaoCodesList,
			bool generateSid,
			bool generateSidTransitions,
			bool generateStar,
			bool generateStarTransitions
			)
		{
			bool retVal;

			try
			{
				var exeConfiguration = ConfigurationManager.OpenExeConfiguration(
					ConfigurationUserLevel.None
					);

				//	text fields
				//

				exeConfiguration.AppSettings.Settings["Kln90BNavDataPath"].Value
					= navDataPath;

				exeConfiguration.AppSettings.Settings[nameof(IcaoCodesList)].Value
					= icaoCodesList;

				//	SID options
				//

				exeConfiguration.AppSettings.Settings[nameof(GenerateSid)].Value
					= generateSid.ToString();

				exeConfiguration.AppSettings.Settings[nameof(GenerateSidTransitions)].Value
					= generateSidTransitions.ToString();

				//	STAR options
				//

				exeConfiguration.AppSettings.Settings[nameof(GenerateStar)].Value
					= generateStar.ToString();

				exeConfiguration.AppSettings.Settings[nameof(GenerateStarTransitions)].Value
					= generateStarTransitions.ToString();

				//	save and update application settings
				//

				exeConfiguration.Save(
					ConfigurationSaveMode.Modified
					);

				ConfigurationManager.RefreshSection(
					"appSettings"
					);

				retVal = true;
			}
			catch (Exception)
			{
				retVal = false;
			}

			return retVal;
		}

		private static bool ReadBoolValue(string paramName)
		{
			bool retVal;

			try
			{
				retVal = bool.Parse(
					ConfigurationManager.AppSettings[paramName]
					);
			}
			catch (Exception)
			{
				retVal = false;
			}

			return retVal;
		}

		private static string ReadStringValue(string paramName)
		{
			string retVal;

			try
			{
				retVal = ConfigurationManager.AppSettings[paramName];
			}
			catch (Exception)
			{
				retVal = string.Empty;
			}

			return retVal;
		}

		private static string BuildXPlaneInstancesFilePath(
			bool buildPrior10VersionInstancesPath
			)
		{
			var xplaneInstancesFilename = (
				buildPrior10VersionInstancesPath
				? "x-plane_install.txt"
				: "x-plane_install_10.txt"
				);

			return Path.Combine(
				Environment.GetFolderPath(
					Environment.SpecialFolder.LocalApplicationData
					),
				xplaneInstancesFilename
				);
		}

		private static string FindFirstNavDataFolderInstance(
			string xplaneInstancesFilePath
			)
		{
			if (!File.Exists(xplaneInstancesFilePath))
			{
				return string.Empty;
			}

			string retVal = null;

			try
			{
				var installedInstances = File.ReadAllLines(
					xplaneInstancesFilePath
					);

				foreach (var someInstance in installedInstances)
				{
					var kln90BNavDataPath = Path.Combine(
						someInstance,
						"KLN 90B Navdata"
						).Replace('/', '\\');

					if (!Directory.Exists(kln90BNavDataPath))
					{
						continue;
					}

					retVal = kln90BNavDataPath;

					break;
				}
			}
			catch (Exception)
			{
				retVal = null;
			}

			return retVal ?? string.Empty;
		}
	}
}
