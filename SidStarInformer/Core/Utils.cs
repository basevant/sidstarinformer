﻿
using System;

namespace SidStarInformer.Core
{
	internal static class Utils
	{
		internal static bool StringsAreEqual(
			string firstString,
			string secondString
			)
			=> (
				0 == string.Compare(
					firstString,
					secondString,
					StringComparison.OrdinalIgnoreCase
					)
			);

		internal static bool WeHaveLatinCharacter(char someCharacter)
			=> (
					(
						(someCharacter >= 'a')
						&& (someCharacter <= 'z')
					)
					||
					(
						(someCharacter >= 'A')
						&& (someCharacter <= 'Z')
					)
				);
	}
}
