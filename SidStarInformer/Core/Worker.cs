﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SidStarInformer.Models;

namespace SidStarInformer.Core
{
	internal static class Worker
	{
		internal static IEnumerable<AirportInfoModel> GenerateData(
			string navDataPath,
			string icaoCodesList,
			bool generateSid,
			bool generateSidTransition,
			bool generateStar,
			bool generateStarTransition
			)
		{
			var airportCodesArray = ParseIcaoCodesList(icaoCodesList).ToArray();

			var retVal = new List<AirportInfoModel>(
				airportCodesArray.Select(
					someIcaoCode => ProcessAirport(
						navDataPath,
						someIcaoCode,
						generateSid,
						generateSidTransition,
						generateStar,
						generateStarTransition
					)
				)
			);

			return retVal.ToArray();
		}

		internal static IEnumerable<string> ParseIcaoCodesList(
			string icaoCodesList
			)
		{
			var retVal = new List<string>();

			var icaoCodeBuilder = new StringBuilder();

			foreach (var someCharacter in icaoCodesList)
			{
				if (Utils.WeHaveLatinCharacter(someCharacter))
				{
					icaoCodeBuilder.Append(someCharacter);
				}
				else
				{
					//	a delimeter was found
					//

					if (icaoCodeBuilder.Length > 0)
					{
						icaoCodeBuilder = new StringBuilder();
					}
				}

				if (4 != icaoCodeBuilder.Length)
				{
					continue;
				}

				//	we get some ICAO code
				//

				retVal.Add(
					icaoCodeBuilder.ToString().ToUpperInvariant()
				);

				icaoCodeBuilder = new StringBuilder();
			}

			return retVal.ToArray();
		}

		private static AirportInfoModel ProcessAirport(
			string navDataPath,
			string airportIcaoCode,
			bool generateSid,
			bool generateSidTransition,
			bool generateStar,
			bool generateStarTransition
			)
		{
			var retVal = new AirportInfoModel(airportIcaoCode);

			if (generateSid)
			{
				retVal.AddRunwaySidPayload(
					GenerateSidStarData(
						navDataPath,
						airportIcaoCode,
						true
					)
				);
			}

			if (generateSidTransition)
			{
				retVal.AddDepartureTransitionsPayload(
					GenerateTransitionData(
						navDataPath,
						airportIcaoCode,
						true
						)
					);
			}

			if (generateStar)
			{
				retVal.AddRunwayStarPayload(
					GenerateSidStarData(
						navDataPath,
						airportIcaoCode,
						false
					)
				);
			}

			if (generateStarTransition)
			{
				retVal.AddArrivalTransitionsPayload(
					GenerateTransitionData(
						navDataPath,
						airportIcaoCode,
						false
						)
					);
			}

			return retVal;
		}

		private static KeyValuePayloadModel GenerateSidStarData(
			string navDataPath,
			string airportIcaoCode,
			bool generateSid
			)
		{
			var retVal = new KeyValuePayloadModel();

			try
			{
				var sidOrStarFilename = GetSidStarFilename(
					airportIcaoCode
					, generateSid
					);

				var sidOrStarFullPath = Path.Combine(
					navDataPath,
					sidOrStarFilename
					);

				if (File.Exists(sidOrStarFullPath))
				{
					var includedSchemas = new List<string>();

					foreach (var someRoutePoint in File.ReadAllLines(sidOrStarFullPath))
					{
						var lineParts = someRoutePoint.Split(',');
						if (lineParts.Length < 5)
						{
							continue;
						}

						var runwayNumber = lineParts[1];
						var schemaName = lineParts[0];
						var portInUpperCase = airportIcaoCode.ToUpperInvariant();

						//	list item format
						//	UUEE 06L BES06D
						//
						var portRunwaySchema = (
							$"{portInUpperCase} {runwayNumber} {schemaName}"
							);

						if (includedSchemas.Contains(portRunwaySchema))
						{
							continue;
						}

						//	to avoid duplicates
						//
						includedSchemas.Add(portRunwaySchema);

						retVal.AddPayload(
							runwayNumber,
							schemaName
							);
					}
				}
			}
			catch (Exception)
			{
				return retVal.OrderByKey();
			}

			return retVal.OrderByKey();
		}

		private static KeyValuePayloadModel GenerateTransitionData(
			string navDataPath,
			string airportIcaoCode,
			bool generateDepartureTransition
			)
		{
			var transitionFilename = GetDepArrTransitionFilename(
				airportIcaoCode,
				generateDepartureTransition
				);

			var transitionFilenameFullPath = Path.Combine(
				navDataPath,
				transitionFilename
				);

			var retVal = new KeyValuePayloadModel();

			if (!File.Exists(transitionFilenameFullPath))
			{
				return retVal;
			}

			try
			{
				var processedTransitions = new List<string>();

				foreach (var someLine in File.ReadAllLines(transitionFilenameFullPath))
				{
					var lineParts = someLine.Split(',');
					if (lineParts.Length < 3)
					{
						continue;
					}

					var transitionName = lineParts[0];
					var sidStarName = lineParts[1];
					var transitionPoint = lineParts[2];

					if (!processedTransitions.Contains(transitionName))
					{
						//	a transition does not contain the first/last point,
						//	we should add it manually
						//

						var firstOrLastSchemePoint = GetSchemeFirstOrLastPoint(
							navDataPath,
							airportIcaoCode,
							sidStarName,
							generateDepartureTransition
							);

						if (!string.IsNullOrEmpty(firstOrLastSchemePoint))
						{
							retVal.AddPayload(
								transitionName,
								firstOrLastSchemePoint
								);
						}

						processedTransitions.Add(transitionName);
					}

					retVal.AddPayload(
						transitionName,
						transitionPoint
						);
				}
			}
			catch (Exception)
			{
				return retVal.OrderByKey();
			}

			return retVal.OrderByKey();
		}

		private static string GetSchemeFirstOrLastPoint(
			string navDataPath,
			string airportIcaoCode,
			string sidStarName,
			bool useSidScheme
			)
		{
			var sidStarFilename = GetSidStarFilename(
				airportIcaoCode
				, useSidScheme
				);

			var sidStarFilenameFullPath = Path.Combine(
				navDataPath,
				sidStarFilename
				);

			if (!File.Exists(sidStarFilenameFullPath))
			{
				return string.Empty;
			}

			var schemeWasFoundBefore = false;
			var retVal = string.Empty;

			foreach (var someLine in File.ReadAllLines(sidStarFilenameFullPath))
			{
				var lineParts = someLine.Split(',');
				if (lineParts.Length < 3)
				{
					continue;
				}

				var someSidStarName = lineParts[0];

				var foundTargetScheme = Utils.StringsAreEqual(
					someSidStarName,
					sidStarName
					);

				if (
					foundTargetScheme
					&& !useSidScheme
					)
				{
					//	for STAR we return first point
					//

					break;
				}

				if (
					!foundTargetScheme
					&& useSidScheme
					&& schemeWasFoundBefore
					)
				{
					//	for SID we return last point
					//

					break;
				}

				schemeWasFoundBefore = (
					schemeWasFoundBefore
					|| foundTargetScheme
					);

				retVal = lineParts[2];
			}

			return retVal;
		}

		private static string GetSidStarFolderName(
			bool generateSidFolderName
			)
			=> (
				generateSidFolderName ? "Sids" : "Stars"
			);

		private static string GetSidStarFilename(
			string airportIcaoCode,
			bool generateSidFilename
			)
			=> Path.Combine(
				GetSidStarFolderName(generateSidFilename),
				$"{airportIcaoCode}.{(generateSidFilename ? "sid" : "star")}"
				);

		private static string GetDepArrTransitionFilename(
			string airportIcaoCode,
			bool generateSidFilename
			)
			=> Path.Combine(
				GetSidStarFolderName(generateSidFilename),
				$"{airportIcaoCode}.{(generateSidFilename ? "sidtrs" : "startrs")}"
				);
	}
}
