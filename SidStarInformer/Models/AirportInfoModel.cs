﻿
namespace SidStarInformer.Models
{
	internal sealed class AirportInfoModel
	{
		internal string IcaoCode { get; }

		internal KeyValuePayloadModel RunwaySidPayload { get; }

		internal KeyValuePayloadModel DepartureTransitionsPayload { get; }

		internal KeyValuePayloadModel RunwayStarPayload { get; }

		internal KeyValuePayloadModel ArrivalTransitionsPayload { get; }

		internal AirportInfoModel(string airportIcaoCode)
		{
			IcaoCode = airportIcaoCode;

			RunwaySidPayload = new KeyValuePayloadModel();
			DepartureTransitionsPayload = new KeyValuePayloadModel();

			RunwayStarPayload = new KeyValuePayloadModel();
			ArrivalTransitionsPayload = new KeyValuePayloadModel();
		}

		internal void AddRunwaySidPayload(
			KeyValuePayloadModel runwayPayload
			)
		{
			RunwaySidPayload.AddPayload(
				runwayPayload
				);
		}

		internal void AddDepartureTransitionsPayload(
			KeyValuePayloadModel transitionPayload
			)
		{
			DepartureTransitionsPayload.AddPayload(
				transitionPayload
				);
		}

		internal void AddRunwayStarPayload(
			KeyValuePayloadModel runwayPayload
			)
		{
			RunwayStarPayload.AddPayload(
				runwayPayload
				);
		}

		internal void AddArrivalTransitionsPayload(
			KeyValuePayloadModel transitionPayload
			)
		{
			ArrivalTransitionsPayload.AddPayload(
				transitionPayload
				);
		}
	}
}
