﻿using System.Collections.Generic;
using System.Linq;

namespace SidStarInformer.Models
{
	internal sealed class KeyValuePayloadModel : Dictionary<string, List<string>>
	{
		public KeyValuePayloadModel()
		{ }

		private KeyValuePayloadModel(
			IEnumerable<KeyValuePair<string, List<string>>> value
			)
			: base(value.ToDictionary(v => v.Key, v => v.Value))
		{
		}

		internal void AddPayload(
			string key,
			string payloadValue
			)
		{
			if (ContainsKey(key))
			{
				var thisKey = this[key];

				if (!thisKey.Contains(payloadValue))
				{
					thisKey.Add(payloadValue);
				}
			}
			else
			{
				this[key] = new List<string> { payloadValue };
			}
		}

		internal void AddPayload(KeyValuePayloadModel payloadModel)
		{
			foreach (var somePayload in payloadModel)
			{
				AddPayload(
					somePayload.Key,
					somePayload.Value
					);
			}
		}

		internal KeyValuePayloadModel OrderByKey()
			=> new KeyValuePayloadModel(this.OrderBy(zz => zz.Key));

		private void AddPayload(
			string key,
			IEnumerable<string> payloadValue
			)
		{
			if (ContainsKey(key))
			{
				this[key] = this[key].Union(payloadValue).ToList();
			}
			else
			{
				this[key] = new List<string>(payloadValue);
			}
		}
	}
}
