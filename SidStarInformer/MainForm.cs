﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SidStarInformer.Configuration;
using SidStarInformer.Core;
using SidStarInformer.Models;
using SidStarInformer.Properties;

namespace SidStarInformer
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			LoadUiSettings();
		}

		private void LoadUiSettings()
		{
			txtNavDataPath.Text = Configurator.GetKln90BNavDataPath();
			txtIcaoCodes.Text = Configurator.IcaoCodesList;

			chckGenerateSid.Checked = Configurator.GenerateSid;
			chckGenerateSidTransitions.Checked = Configurator.GenerateSidTransitions;

			chckGenerateStar.Checked = Configurator.GenerateStar;
			chckGenerateStarTransitions.Checked = Configurator.GenerateStarTransitions;
		}

		private void btnGenerate_Click(object sender, EventArgs e)
		{
			var errorMessages = ValidateRequestedCriteria();
			if (!string.IsNullOrEmpty(errorMessages))
			{
				ShowWarningMessageBox(errorMessages);

				return;
			}

			var generatedData = Worker.GenerateData(
				txtNavDataPath.Text,
				txtIcaoCodes.Text,
				chckGenerateSid.Checked,
				chckGenerateSidTransitions.Checked,
				chckGenerateStar.Checked,
				chckGenerateStarTransitions.Checked
				);

			GenerateAndShowReport(generatedData);

			var appSettingsWereUpdated = Configurator.SaveUiSettings(
				txtNavDataPath.Text,
				txtIcaoCodes.Text,
				chckGenerateSid.Checked,
				chckGenerateSidTransitions.Checked,
				chckGenerateStar.Checked,
				chckGenerateStarTransitions.Checked
				);

			if (!appSettingsWereUpdated)
			{
				ShowWarningMessageBox(
					Resources.UnableToUpdateAppSettingsMessage
					);
			}
		}

		private void btnBrowseForNavDataPath_Click(object sender, EventArgs e)
		{
			string selectedPath;

			using (
				var folderBrowserDialog = new FolderBrowserDialog
				{
					RootFolder = Environment.SpecialFolder.MyComputer,
					Description = Resources.BrowseForNavDataDialogDescription,
					ShowNewFolderButton = false
				}
				)
			{
				if (DialogResult.OK != folderBrowserDialog.ShowDialog())
				{
					return;
				}

				selectedPath = folderBrowserDialog.SelectedPath;
			}

			//	validate, basically, the selected folder
			//

			if (!Directory.Exists(selectedPath ?? string.Empty))
			{
				selectedPath = string.Empty;
			}

			txtNavDataPath.Text = selectedPath;
		}

		private static string FormatNavData(AirportInfoModel airportModel)
		{
			var retValBuilder = new StringBuilder();

			retValBuilder.AppendLine(
				GenerateOuterDelimeter()
				);

			retValBuilder.AppendLine(airportModel.IcaoCode);

			if (airportModel.RunwaySidPayload.Any())
			{
				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);

				retValBuilder.AppendLine("SID");

				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);
			}

			foreach (var runwayInfo in airportModel.RunwaySidPayload)
			{
				retValBuilder.AppendLine(
					$"{runwayInfo.Key} {FormatSidStarPayload(runwayInfo.Value)}"
					);
			}

			if (airportModel.DepartureTransitionsPayload.Any())
			{
				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);

				retValBuilder.AppendLine("Departure transitions");

				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);
			}

			foreach (var transitionInfo in airportModel.DepartureTransitionsPayload)
			{
				retValBuilder.AppendLine(
					$"{transitionInfo.Key} {FormatTransitionPayload(transitionInfo.Value)}"
					);
			}

			if (airportModel.RunwayStarPayload.Any())
			{
				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);

				retValBuilder.AppendLine("STAR");

				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);
			}

			foreach (var transitionInfo in airportModel.RunwayStarPayload)
			{
				retValBuilder.AppendLine(
					$"{transitionInfo.Key} {FormatSidStarPayload(transitionInfo.Value)}"
					);
			}

			if (airportModel.ArrivalTransitionsPayload.Any())
			{
				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);

				retValBuilder.AppendLine("Arrival transitions");

				retValBuilder.AppendLine(
					GenerateInnerDelimeter()
					);
			}

			foreach (var transitionInfo in airportModel.ArrivalTransitionsPayload)
			{
				retValBuilder.AppendLine(
					$"{transitionInfo.Key} {FormatTransitionPayload(transitionInfo.Value)}"
					);
			}

			return retValBuilder.ToString();
		}

		private static string FormatSidStarPayload(IEnumerable<string> someList)
			=> string.Join("/", someList.ToArray());

		private static string FormatTransitionPayload(IEnumerable<string> someList)
			=> string.Join(" ", someList.ToArray());

		private static string GenerateDelimeterLine(char symbol, int lineLength)
			=> new string(new[] { symbol }).PadRight(lineLength, symbol);

		private static string GenerateOuterDelimeter()
			=> GenerateDelimeterLine('=', 50);

		private static string GenerateInnerDelimeter()
			=> GenerateDelimeterLine('-', 50);

		private void ShowWarningMessageBox(
			string messageBoxText
			)
		{
			MessageBox.Show(
				messageBoxText,
				Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Warning
				);
		}

		private string ValidateRequestedCriteria()
		{
			var retValBuilder = new StringBuilder();
			var validationIntroWasAdded = false;

			//	check the nav data folder
			//

			if (!Directory.Exists(txtNavDataPath.Text))
			{
				retValBuilder.AppendLine(
					Resources.ValidationWeHaveProblem
					);

				validationIntroWasAdded = true;

				retValBuilder.AppendLine(
					$"- {Resources.ValidationInvalidNavDataDirectory}"
					);
			}

			//	check the requested ICAO codes list
			//

			if (!Worker.ParseIcaoCodesList(txtIcaoCodes.Text).Any())
			{
				if (!validationIntroWasAdded)
				{
					retValBuilder.AppendLine(
						Resources.ValidationWeHaveProblem
					);

					validationIntroWasAdded = true;
				}

				retValBuilder.AppendLine(
					$"- {Resources.ValidationUnableToGetAirportsList}"
					);
			}

			//	check the requested options
			//
			var anyOptionCheckBoxSet = (
				chckGenerateSid.Checked
				|| chckGenerateSidTransitions.Checked
				|| chckGenerateStar.Checked
				|| chckGenerateStarTransitions.Checked
				);

			if (!anyOptionCheckBoxSet)
			{
				if (!validationIntroWasAdded)
				{
					retValBuilder.AppendLine(
						Resources.ValidationWeHaveProblem
						);
				}

				retValBuilder.AppendLine(
					$"- {Resources.ValidationWeHaveNoSelectedOptions}"
					);
			}

			return retValBuilder.ToString();
		}

		private static void GenerateAndShowReport(
			IEnumerable<AirportInfoModel> generatedData
			)
		{
			var reportBuilder = new StringBuilder();
			foreach (var someAirportData in generatedData)
			{
				reportBuilder.AppendLine(
					FormatNavData(someAirportData)
					);
			}

			//	save data to the %Temp% folder
			//
			var reportFileFullPath = Path.Combine(
				Path.GetTempPath(),
				"SitStarInformerReport.txt"
				);

			File.WriteAllText(
				reportFileFullPath,
				reportBuilder.ToString()
				);

			Process.Start(reportFileFullPath);
		}
	}
}
